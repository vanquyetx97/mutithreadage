package com.company;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static int count;

    public static void main(String[] args) {
        String pathDefault = "D:\\test-text\\";
        List<String> paths = Arrays.asList(pathDefault + "dad.txt", pathDefault + "mom.txt", pathDefault + "ubnd.txt");

        DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        ExecutorService taskExecutor = Executors.newFixedThreadPool(3);
        for (String path : paths) {
            taskExecutor.execute(() -> {
                File file = new File(path);
                try {
                    FileReader fileReader = new FileReader(file);
                    String value = "";
                    Integer old;
                    Scanner myReader = new Scanner(fileReader);
                    while (myReader.hasNextLine()) {
                        value = myReader.nextLine();
                    }
                    myReader.close();
                    if (path.contains("dad")) {
                        old = Integer.valueOf(value);
                    } else if (path.contains("mom")) {
                        old = Calendar.getInstance().get(Calendar.YEAR) - Integer.valueOf(value);
                    } else {
                        LocalDate date = LocalDate.parse(value, sdf);
                        old = Period.between(date, LocalDate.now()).getYears();
                    }
                    if (old.equals(21)) {
                        System.out.println(path);
                        checkResult();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        taskExecutor.shutdown();

    }

    synchronized private static void checkResult() throws IOException, InterruptedException {
        count++;
        if (count >= 2) {
            System.out.println("ok");
            File file = new File("D:\\test-text\\result.txt");
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("ok");
            bw.close();
            fw.close();
        }
    }
}
